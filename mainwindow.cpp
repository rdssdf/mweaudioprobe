#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDir>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_ui(new Ui::MainWindow)
    , m_player(new QMediaPlayer(this))
    , m_probe(new QAudioProbe(this))
{
    m_ui->setupUi(this);
    m_ui->pushButton_Play->setEnabled(false);

    m_probe->setSource(m_player);
    connect(m_probe, SIGNAL(audioBufferProbed(const QAudioBuffer&)), this, SLOT(processAudioProbe(const QAudioBuffer&)));
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::on_pushButton_OpenFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QDir::homePath(), "*.wav *.mp3 *.mp4");
    QUrl url = QUrl::fromLocalFile(fileName);
    QMediaContent media(url);
    m_player->setMedia(media);
    m_ui->pushButton_Play->setEnabled(true);
}

void MainWindow::on_pushButton_Play_clicked()
{
    m_player->play();
}

void MainWindow::processAudioProbe(const QAudioBuffer& buffer)
{
    qDebug() << "audio buffer probed. bytes in buffer:" << buffer.byteCount();
}
