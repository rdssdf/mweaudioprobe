#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QAudioProbe>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_OpenFile_clicked();
    void on_pushButton_Play_clicked();
    void processAudioProbe(const QAudioBuffer&);

private:
    Ui::MainWindow* m_ui;
    QMediaPlayer*   m_player;
    QAudioProbe*    m_probe;
};

#endif // MAINWINDOW_H
